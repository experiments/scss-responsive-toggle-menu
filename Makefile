SCSS_SOURCE_DIR := scss
SCSS_SOURCES	:= $(wildcard $(SCSS_SOURCE_DIR)/[^_]*.scss)

CSS_DEST_DIR := css
CSS_COMPILED := $(SCSS_SOURCES:$(SCSS_SOURCE_DIR)/%.scss=%.css)

SCSS_OUTPUT_TYPE := expanded

SCSS_BIN := sassc
SCSS_FLAGS := -t $(SCSS_OUTPUT_TYPE)

update: $(CSS_COMPILED)

clean:
	rm -rf $(CSS_DEST_DIR) .sass-cache

%.css: $(SCSS_SOURCE_DIR)/%.scss
	@[ -d $(CSS_DEST_DIR) ] || mkdir -p $(CSS_DEST_DIR)
	$(SCSS_BIN) $(SCSS_FLAGS) $< $(CSS_DEST_DIR)/$@
